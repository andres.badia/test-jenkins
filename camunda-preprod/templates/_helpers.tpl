{{/*
Expand the name of the chart.
*/}}
{{- define "camunda-preprod.name" -}}
{{- default .Chart.Name .Values.camunda.preprod.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "camunda-preprod.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Image constructor.
*/}}

{{- define "camunda-preprod.image" }}
{{- $registryName := .Values.registry -}}
{{- $imageName := .Values.camunda.preprod.image.name -}}
{{- $imageTag := .Values.camunda.preprod.image.tag | toString -}}
{{- printf "%s/%s:%s" $registryName $imageName $imageTag -}}
{{- end -}}

{{/*
Renders a value that contains template.
*/}}
{{- define "camunda-preprod.tplValue" -}}
    {{- if typeIs "string" .value }}
        {{- tpl .value .context }}
    {{- else }}
        {{- tpl (.value | toYaml) .context }}
    {{- end }}
{{- end -}}