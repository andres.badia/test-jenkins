{{/*
Expand the name of the chart.
*/}}
{{- define "camunda-prod.name" -}}
{{- default .Chart.Name .Values.camunda.prod.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "camunda-prod.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Image constructor.
*/}}

{{- define "camunda-prod.image" }}
{{- $registryName := .Values.registry -}}
{{- $imageName := .Values.camunda.prod.image.name -}}
{{- $imageTag := .Values.camunda.prod.image.tag | toString -}}
{{- printf "%s/%s:%s" $registryName $imageName $imageTag -}}
{{- end -}}

{{/*
Renders a value that contains template.
*/}}
{{- define "camunda-prod.tplValue" -}}
    {{- if typeIs "string" .value }}
        {{- tpl .value .context }}
    {{- else }}
        {{- tpl (.value | toYaml) .context }}
    {{- end }}
{{- end -}}